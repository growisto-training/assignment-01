function multiplyMe (multiplier) {
  return function (x) {
    return x * multiplier
  }
}

const doubleMe = multiplyMe(2)
console.log(doubleMe(45))
console.log(multiplyMe(34)(3))