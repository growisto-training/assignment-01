const library = [
  {
    author: 'Bill Gates',
    title: 'The Road Ahead',
    libraryID: 1254
  },
  {
    author: 'Steve Jobs',
    title: 'Walter Isaacson',
    libraryID: 4264
  },
  {
    author: 'Suzanne Collins',
    title: 'Mockingjay: The Final Book of The Hunger Games',
    libraryID: 3245
  }
]

const x = function (library, callback) {
  return callback(library)
}
function sortBook (books) {
  return books.sort(function (a, b) {
    if (a.title > b.title) return 1
    else return -1
  }
  )
}

console.log(x(library, sortBook))
