const myArray = [
  1, 2, 3, 7, 10, 13, 19, 22
]

const oddArray = function (arr) {
  return arr.filter(ele => (ele & 1) === 1)
}
const evenArray = function (arr) {
  return arr.filter(ele => (ele & 1) === 0)
}

function myFunc (arr, callback) {
  console.log(arr)
  return callback(arr)
}
console.log(Array)
console.log(myFunc(myArray, oddArray))
console.log(myFunc(myArray, evenArray))
