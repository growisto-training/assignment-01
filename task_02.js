const formater = function (date, converter) {
  const [hour, minute, secondsShift] = date.split(':')
  const [seconds, shift] = secondsShift.split(' ')
  return converter(Number(hour), shift) + ':' + minute + ':' + seconds
}

const converter = function (hour, shift) {
  if (hour === 12 && shift === 'AM') {
    return hour - 12
  } else if (hour === 12 && shift === 'PM') {
    return hour
  } else if (hour < 12 && shift === 'AM') {
    return hour
  } else if (hour < 12 && shift === 'PM') {
    return hour + 12
  }
}

console.log(formater('12:20:00 AM', converter))
